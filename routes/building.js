const express = require('express')
const router = express.Router()
const Building = require('../model/Building')

const getBuildings = async function (req, res, next) {
  const buildings = await Building.find({})
  res.status(200).json(buildings)
}

const getBuilding = async function (req, res, next) {
  const building = await Building.findById(req.params.id)

  res.status(200).json(building)
}

const addBuldings = async function (req, res, next) {
  try {
    const building = new Building(
      {
        name: req.body.name,
        level: req.body.level
      }
    )
    await building.save()
    res.status(201).send(building)
  } catch (e) {
    res.status(409).json({
      massage: e.massage
    })
  }
}

const updateBuilding = async function (req, res, next) {
  const updated = await Building.findByIdAndUpdate(req.params.id, req.body, { new: true })
  if (updated !== null) { res.status(200).json(updated) } else {
    res.status(404).json({
      massage: 'not found Can\'t delete' + req.params.id
    })
  }
}

const updatePartialBuilding = async function (req, res, next) {
  const updated = await Building.findByIdAndUpdate(req.params.id, req.body, { new: true })

  if (updated !== null) { res.status(200).json(updated) } else {
    res.status(404).json({
      massage: 'not found Can\'t delete' + req.params.id
    })
  }
}

const deleteBuilding = async function (req, res, next) {
  await Building.findByIdAndDelete(req.params.id)
  //  res.status(200) ไม่ Send
  res.status(200).json({
    massage: 'delete success'
  })
}

/* GET users listing. */
router.get('/', getBuildings)
router.post('/', addBuldings)
router.get('/:id', getBuilding)
router.put('/:id', updateBuilding)
router.patch('/:id', updatePartialBuilding)
router.delete('/:id', deleteBuilding)

module.exports = router

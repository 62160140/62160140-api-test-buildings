const mongoose = require('mongoose')
const Building = require('../model/Building')

mongoose.connect('mongodb://localhost:27017/example')

/* ----------------------- clearDB ---------------------- */
async function clearDB () {
  await Building.deleteMany({})
}

/* ------------------------ main ------------------------ */
async function main () {
  await clearDB()
  /* --------------------- insertMany --------------------- */
  await Building.insertMany([
    {
      name: 'ตึกคณะวิทยาการสารสนเทศ',
      level: 11
    },
    {
      name: 'ตึกสิรินธร',
      level: 5
    }
  ]
  )
}

main().then(function () {
  console.log('Finish')
})

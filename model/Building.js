const mongoose = require('mongoose')

const buildingSchema = mongoose.Schema({
  name: { type: String, unique: true },
  level: Number
})

module.exports = mongoose.model('Building', buildingSchema)
